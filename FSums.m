(*   Copyright 2009-2013 Flavia Stan <flavia.stan@gmail.com> *)
(* Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.  *)

(* A MMA package for determining inhomogeneous recurrences  *)
(* for nested sums over hypergeometric terms with nonstandard boundary conditions  *)
(* by Flavia Stan *)

(* This package works best in combination with   *)
(* the RISC Algorithmic Combinatorics Software Package MultiSum  

   http://www.risc.jku.at/research/combinat/software/

*)

FSums`Private`Version = "0.3  2009-02-02";
(* for MMA 6.0 *)


If[FreeQ[$ContextPath,"MultiSum`"],
    Print["First load the MultiSum package for the computation of certificate recurrences."];
    Abort[]];


BeginPackage["FSums`", {"RISCComb`", "MultiSum`"}];

UseMultiSum::usage = "UseMultiSum[fIn, N, listSumVars] computes a recurrence
    by calling the FindRecurrence and FindStructureSet functions from the package MultSum.
    The list of summation variables can be reordered later by the Arrange functions."

ShiftToPositive::usage = "ShiftToPositive[rec, N, varList] uses the ShiftRecurrence command from the package MultiSum.m
    to shift the recurrence rec such that it contains only positive shifts"

Arrange::usage = "Arrange[rec, range] arranges the notation for the F's appearing in rec
    such we have F[N, listSumVars] where listSumVars is given by range.
    Is used if the certificate recurrence was computed using another order of variables then the one given by the range."

InhRecurrence::usage = " InhRecurrence[rec, N, range, CoeffBound -> const] delivers a inhomogeneous recurrence starting with the certificate recurrence rec in the varaible N. If const>0 the function SplitLargeCoeff is applied for large polynomial coefficients inside de delta parts."

CoeffBound::usage = "Option for the InhRecurrence function. Default value is 0 to obtain the smallest number of FSums in the inhomogeneous part of the recurrence."

FSum::usage = "FSum[summand, range] structure for the nested sums where the range is given as
    a list of the form {{sig, 0, Infinity}, {j,0,N-2}, {k,0,j+1},{l,0,N-k}}"

SubstituteSummandInRec::usage = "SubstituteSummandInRec[rec, fIn, N, listSumVars] substitutes the summand fIn
    as F[N, listSumVars] in the recurrence rec"

Begin["`Private`"];


Global`$PrintConst=1;

Unprotect[UseMultiSum, ShiftToPositive, SubstituteSummandInRec, CompensateTerms, NewSums, TransDeltas, AddDeltaTerms, InhRecurrence, SplitLargeCoeff, Arrange, Grade, FactorOutGCD, ShiftQuotient, MyPrint, MySimplify, RecOrderFct, MyOutCoeff, ExpandFinBSums, PlusTermsFct, FinRangeDeltas, DeltaList, ListOfRangeVars, TrivialSums, SelectRec];


UseMultiSum[fIn_,N_,listSumVars_List]:=Module[
    {strSet,setOfRec,rec},
    
    MyPrint["use FindStructureSet from the package  MultiSum..",1];

    strSet = FindStructureSet[fIn, N, listSumVars, 1];

    If[Head[strSet]=!=List || strSet == {},
       MyPrint["Trouble structure set!!", 1];
       strSet = FindStructureSet[fIn, N, listSumVars];
      ];

    
    MyPrint[" -> structure set: ", strSet[[1]],2];

    MyPrint["use FindRecurrence from the package MultiSum..", 1];
    
    setOfRec = 
    FindRecurrence[fIn, N, listSumVars, strSet[[1]], 1, WZ -> True];

    rec = SelectRec[setOfRec];
    rec = ShiftToPositive[rec, N, listSumVars];
    MyPrint[" ->  rec = ", rec,3];
    rec
                     ];



ShiftToPositive[rec_, N_, varList_List] := Module[
    {shiftList, i, FList, vars,deltaList, listDeltaVars},
    
    vars = Prepend[varList, N];
    
    FList = Cases[rec, F[__], Infinity] // Union;
    
    shiftList = Table[{vars[[i]], vars[[i]] - Simplify[Min[FList[[All, i]]]]}
                      ,{i,1, Length[vars]}];

    MyPrint["use MultiSum to shift the recurrence with {var,shift}: ", shiftList, 1];
    ShiftRecurrence[rec, shiftList]
                                                 ];


SubstituteSummandInRec[rec_,fIn_,N_,listSumVars_List]:=Module[
    {recFinal,a},
    
    recFinal =  rec /. F[a__] :> 
    Simplify[ fIn /. MapThread[Rule, {Prepend[listSumVars, N], {a}}]];
    recFinal = recFinal /. FSum[0, a__] -> 0;
    recFinal = FactorOutGCD[recFinal][[1,1]];
    recFinal
                                                             ];



CompensateTerms[fIn_, rangeIn_List, n_, shift_Integer] := Module[
    {newterms, result, innerCompensated, remRange, curRange, i, j, rest, nsum},
    
    MyPrint["for shift = ", shift, " compensate terms in ", n ," for the range: ",  rangeIn,3 ];
    
    If[rangeIn === {},
       Return[{{fIn /. n -> (n + shift), {}}}];
      ];

    curRange = rangeIn[[1]]; (*the most outer sum*)
    remRange = Drop[rangeIn, 1]; (*all the inner sums*)
    
    MyPrint["current Sum: ", curRange, " remaining Sums: ", remRange, 3];

    result={};

    If[!FreeQ[curRange[[3]], n],
        
        If[ Coefficient[curRange[[3]], n] > 0,
            result = Table[
                {-fIn/.n->(n+shift),remRange}/. (curRange[[1]] -> (curRange[[3]] + j))
                ,{j, 1, shift}];
            MyPrint["newterms 1 = ", result,3],
            result = Table[
                {fIn/.n->(n+shift),remRange}/. (curRange[[1]] -> (curRange[[3]] - j))
                ,{j, 0, shift - 1}];            
            MyPrint["newterms 2 = ", result, 3];
          ]
      ];
            
    innerCompensated = CompensateTerms[fIn, remRange, n, shift];

    rest = Table[{innerCompensated[[i, 1]], 
                  Prepend[innerCompensated[[i, 2]], curRange/.n->(n+shift)]}
                 ,{i, 1, Length[innerCompensated]}];
    
    result = Join[rest, result];
    result
                                                                ];



NewSums[fIn_, rangeIn_List, N_, shift_Integer] :=Module[
    {sumList},
    
    sumList = CompensateTerms[fIn, rangeIn, N, shift];

    sumList = Map[Apply[FSum, #] &, sumList];
    sumList = sumList /. FSum[a_, {}] -> a;


    MyPrint["   -> compensating sums for  ", SUM[N+shift],2];
          
    If[Length[sumList] > 1,
       SUM[N + shift] + Apply[Plus, Drop[sumList, 1]], 
       SUM[N + shift]
      ]
                                                       ];



TransDeltas[rec_, rangeIn_List] := Module[
    {result, deltaList, listDeltaVars, insideDelta, var, rangeOfVar,
     lb, ub, subRange, i, j, listResultSums},
    
    deltaList = DeltaList[rec];
        
    listDeltaVars = Table[deltaList[[i, 1]], {i, 1, Length[deltaList]}];
    
    result = Table[
      
        (* vars that change for each elem of the table *)
        
        insideDelta = deltaList[[i, 2]]; var = deltaList[[i, 1]];
        rangeOfVar = Select[ rangeIn, (#[[1]] === var) &][[1]];
        lb = rangeOfVar[[2]]; ub = rangeOfVar[[3]];
        subRange = Select[ rangeIn, (#[[1]] =!= var) &];
        
        { 
            If[ub === Infinity, 0,
               {insideDelta, subRange} /. var -> ub + 1
              ]
            ,{-insideDelta, subRange} /. var -> lb
        }
        , {i, 1, Length[deltaList]}];
    
    result = Select[Flatten[result, 1], # =!= 0 &];

    MyPrint["delta parts after substitution:", result,3]; 
    
    listResultSums = PlusTermsFct[result];
    
    MyPrint[" from substituting var= lb and var=ub+1 ->  ", listResultSums,3];
    
    Map[Apply[FSum, #]&, listResultSums]
    
                                         ];


(*calling CompensateTerms to determine the telescoping compensating terms*)
AddDeltaTerms[rec_, rangeIn_List] := Module[
    {result, insideDelta, var, rangeOfVar, subRange, newRange, pos, 
     listCTerms, deltaList, listVars, i, j, l, finRange},

    listVars = ListOfRangeVars[rangeIn];
    deltaList = DeltaList[rec];
    
    result=Table[ (* vars for each i *)

                  insideDelta = deltaList[[i, 2]]; var = deltaList[[i, 1]]; 
                  rangeOfVar = Select[ rangeIn, (#[[1]] === var) &][[1]]; 
                  subRange = Select[ rangeIn, (#[[1]] =!= var) &]; 
                  newRange = {var, Simplify[rangeOfVar[[2]] + 1], 
                              Simplify[rangeOfVar[[3]] + 1]};
        
                  MyPrint["for ", var, "  the newRange is: ", newRange, 3]; 
        
                  listCTerms = Drop[CompensateTerms[
                      insideDelta /. var -> (var - 1), 
                      subRange /. var -> (var - 1), var, 1]
                                    ,1];

                  listCTerms = PlusTermsFct[listCTerms];
                  
                  Table[finRange =Table[
                      If[listVars[[l]] === var,{newRange} 
                         ,Select[listCTerms[[j, 2]],(#[[1]] === listVars[[l]]) &]
                        ], {l, 1, Length[listVars]}];
          
                        finRange = Flatten[finRange, 1];

                        {listCTerms[[j, 1]], finRange}

                        ,{j, 1, Length[listCTerms]}
                       ]

                  ,{i, 1,Length[deltaList]}
                ];
    
    result = Flatten[result, 1];
    
    MyPrint["  -> telescoping correction sums --> ", result, 3];
    
    Map[Apply[FSum, #] &, result]
    
                                       ];


Options[InhRecurrence] = { CoeffBound -> 0};


InhRecurrence[rec_, N_, range_List,  opt___Rule] := Module[
    {list, lhs, rhs, addrhs, f, v, r, i, listSumVars, deltaList},

    CoeffSplitBound  = CoeffBound /. {opt} /. Options[InhRecurrence];
    MyPrint["CoeffBound-> ",CoeffSplitBound, 2];
    
    listSumVars = ListOfRangeVars[range];
    
    lhs =
    rec[[1]]/.F[N + shift_Integer:0, a___] :> NewSums[F[N, a], range, N, shift];

    addrhs = (lhs /. SUM[___] -> 0);
        
    lhs = Collect[lhs - addrhs, SUM[___], Factor];
    
    deltaList = Join[TransDeltas[rec, range], AddDeltaTerms[rec, range]];

    rhs = Apply[Plus, deltaList] - addrhs;   
    rhs = rhs /. FSum[f_, r_] :> TrivialSums[f, r];

    rhs = 
    rhs /. FSum[(f_:1)*F[v__], r_] :> MyOutCoeff[f, F[v], r, listSumVars] ;
        
    rhs = 
    rhs /. FSum[f_, r_] :> Plus @@ Map[Apply[FSum, #] &, ExpandFinBSums[f, r]];

        
    If[CoeffSplitBound>0,
    
       rhs = rhs/. FSum[f_,r_]:> Plus @@ Map[ Apply[FSum, #]&,
                                               SplitLargeCoeff[f, r, CoeffSplitBound]];
      ];

    rhs = rhs /. FSum[0, _] :> 0;
    rhs = Collect[rhs, FSum[__], Factor];

    rhs = rhs /. (f_:1)FSum[(g_:1)*F[v__], r_] :> FSum[g*f*F[v], r];
    
    lhs == rhs
                                             ];



SplitLargeCoeff[fIn_,rangeIn_, bound_]:=Module[
    {coeff, rest, i, max, output, free, nonfree, listVars, free2, nonfree2},
    
    listVars = ListOfRangeVars[rangeIn];
    output={{fIn, rangeIn}};

    If[Length[listVars]>0 && !IntegerQ[fIn/.F[___]->1], 
        
       max=Table[Length[fIn[[i]]], {i,1,Length[fIn]}]//Max;

       MyPrint["in SplitLargeCoeff with max ->", max, 3];

       NoPrint[fIn//InputForm, rangeIn//InputForm];

       If[max>bound,

          coeff=Select[fIn, Length[#]===max&]//Expand;
          MyPrint["coeff= ", coeff, 3];
          rest=Select[fIn, Length[#]<max&];

          free=Select[coeff, FreeQ[#, Apply[Alternatives,listVars]]&]//Factor;
          nonfree=Select[coeff, !FreeQ[#, Apply[Alternatives,listVars]]&]//Factor;
          
          output={{free*rest, rangeIn}, {nonfree*rest, rangeIn}};
          
          If[Length[listVars]>1 && Length[nonfree//Expand]>2*bound,
             
             nonfree=nonfree//Expand;

             free2=Select[nonfree, FreeQ[#, listVars[[1]]]&]//Factor;
             nonfree2=Select[nonfree, !FreeQ[#, listVars[[1]]]&]//Factor;

             output={{free*rest, rangeIn}, {free2*rest, rangeIn}, {nonfree2*rest, rangeIn}};
             
             MyPrint["test = ", coeff -(free+free2+nonfree2)//FullSimplify, 3];

            ];
         ];

      ];
    output
                                              ];


(* determines the grade of the recurrence operator*)
Grade[rec_, N_, param_, rangeIn_List] := Module[
    {i,j, a, b, deltaVars, deltas, finRange, vars, table, min, int, shift},
    
    finRange = Select[rangeIn, #[[3]]=!=Infinity &];

    vars = Table[finRange[[i,1]],{i,1,Length[finRange]}];
    
    deltas = Cases[rec, Delta[__],Infinity];

    deltas= Select[deltas, Length[#[[2]]]>0 &];
   
    shift=0;
    
    If[Length[deltas]>0,
     
       deltas = Table[{deltas[[i,1]], Cases[deltas[[i,2]], F[__],Infinity]}, {i,1,Length[deltas]}];

       If[param===N,

          table=deltas/.F[a_,b___]:>(a-N-Select[{b},!FreeQ[#, Apply[Alternatives, vars]]&]);
          
          table=Table[table[[i]]//Flatten, {i,1,Length[table]}];
                    
          shift=Table[
              Table[
                  If[!FreeQ[table[[i,j]],table[[i,1]]],
                     table[[i,j]]+ table[[i,1]] -1,
                     Select[vars, !FreeQ[table[[i,j]], #]&][[1]]+ table[[i,j]]
                    ]
                  ,{j,2,Length[table[[i]]]}]
              ,{i,1,Length[table]}];
          
          shift=shift//Min;
          shift=-shift;
          MyPrint["shift1= ", shift, 3]

          ,table=deltas/.F[a_,b___]:>(Select[{b},!FreeQ[#, Apply[Alternatives, vars]]&]-vars);
         
          shift=Table[ If[FreeQ[vars,table[[i,1]]], table[[i,2]], table[[i,2]]+1]
                      ,{i,1,Length[table]}]//Flatten//Max;
          MyPrint["shift2= ", shift, 3];
         ];
      ];
    shift
                                                        ];



FactorOutGCD[rec_] := Module[
    {lhs, rhs, gcd, newlhs},
    MyPrint["factor out the GCD -> ",2];
    lhs = rec[[1]];
    rhs = rec[[2]];
    gcd = 1;    

    MyPrint["head of lhs :  ", Head[lhs], 3];
    If[Head[lhs] === Times,
       gcd = lhs/.SUM[__] -> 1;
       lhs = lhs/gcd
            ];

    If[Head[lhs] === Plus,
       lhs= Apply[List, lhs];
       gcd = Apply[PolynomialGCD, lhs];
       lhs=Apply[Plus, lhs/gcd]
       ,lhs
      ];

    MyPrint[" gcd of coeff from lhs of rec :  ", gcd, 3];

    If[gcd =!= 1,
       rhs = If[FreeQ[rhs, FSum],
          rhs/gcd,
          rhs /.FSum[x_, y_] -> FSum[x/gcd, y]
               ]
      ];
   {{lhs == rhs}, {gcd}}
    ];



(*returns FullSimplify[(fIn/.N->N+sh)/fIn]*)
ShiftQuotient[fIn_, N_, shift_] := Module[
    {Npart, tbl, i, final},

    If[shift===0, Return[1]];
    Npart = fIn /. Pochhammer[a_, b_] -> Gamma[a + b]/Gamma[a];
    Npart = Npart /. Binomial[a_, b_] -> Gamma[a + 1]/Gamma[b + 1]/Gamma[a - b + 1];
    
    Npart = Select[Npart,!FreeQ[#, N]&];
    MyPrint[" in ShiftQuotient-> Npart= ", Npart,3];
      
    tbl = If[Head[Npart]===Power||Head[Npart]===Gamma,{Npart},List@@Npart];
   
    tbl= Table[(tbl[[i]]/. N -> (N + shift))/tbl[[i]], {i, 1, Length[tbl]}];
   
    tbl= Table[tbl[[i]]/.{Gamma[a_]^n_ Gamma [b_]^m_ :>
                          If[IntegerQ[a - b] && (m + n == 0)
                             ,(Poch[a, b])^n
                             ,Gamma[a]^n*Gamma[b]^m
                            ]
                          }
               ,{i, 1, Length[tbl]}
              ];
   
    tbl= Table[tbl[[i]] /.{Gamma[a_]/Gamma [b_] :>
                           If[ IntegerQ[a - b]
                               ,Poch[a, b]
                               ,Gamma[a]/Gamma[b]
                             ]
                          }
               ,{i, 1,Length[tbl]}
              ];
   
      
    final = Apply[Times, tbl];
    MyPrint["shifting in ", N, " with ", shift, " result=", final,2];
    final
                                         ];


TrivialSums[fIn_, rangeIn_List] := Module[
    {i},
    
    For[i = 1, i <= Length[rangeIn], i++, 
        If[rangeIn[[i, 2]] - rangeIn[[i, 3]] > 0, Return[0]]
       ];
    FSum[fIn, rangeIn]
    
    ];


ListOfRangeVars[rangeIn_List] := Module[
    {i},
    Table[rangeIn[[i, 1]], {i, 1, Length[rangeIn]}]
                                   ];

DeltaList[rec_] := Module[
    {},
    Cases[rec /. Delta[_, 0] -> 0, Delta[__], Infinity] // Union
                         ];


FinRangeDeltas[rec_, rangeIn_List] := Module[
    {i, deltaList, listDeltaVars},
    
    deltaList = DeltaList[rec];
    
    listDeltaVars = Table[deltaList[[i, 1]], {i, 1, Length[deltaList]}];
    
    infinRange = Select[rangeIn, #[[3]] === Infinity &];
    
    listDeltaVars =
    Complement[listDeltaVars, 
               Table[infinRange[[i, 1]], {i, 1, Length[infinRange]}]];

    Select[deltaList, ! FreeQ[#[[1]], Apply[Alternatives, listDeltaVars]] &]
                                            ];


PlusTermsFct[listTerms_List] := Module[
    {plusTerms, plTermsIntoSums, i, j},
    
    plusTerms = Select[listTerms, (Head[#[[1]]] == Plus) &];
    
    MyPrint["  separte the plus terms :", plusTerms,2];
    
    plTermsIntoSums = Table[Table[{plusTerms[[i, 1, j]], plusTerms[[i, 2]]}
                                  ,{j, 1, Length[plusTerms[[i, 1]]]}]
                            ,{i, 1, Length[plusTerms]}];
    
    plTermsIntoSums = Flatten[plTermsIntoSums, 1];
    
    Join[plTermsIntoSums, Select[listTerms, (Head[#[[1]]] =!= Plus) &]]
                                      ];

ExpandFinBSums[fIn_, rangeIn_] := Module[
    {f = fIn, curRange, remRange, listOfSums, i},
    
    If[rangeIn === {} || Select[rangeIn, IntegerQ[#[[3]]] &] === {},
       Return[{{fIn, rangeIn}}]
      ];
    
    curRange = rangeIn[[1]];
    remRange = Drop[rangeIn, 1];
    
    If[IntegerQ[curRange[[3]]],
      
       Flatten[Table[
           ExpandFinBSums[f, Evaluate[remRange]], Evaluate[curRange]
                    ]
               , 1]
       ,listOfSums = ExpandFinBSums[f, remRange];
      
       Table[{listOfSums[[i, 1]], Prepend[listOfSums[[i, 2]], curRange]}
             ,{i, 1, Length[listOfSums]}]
      ]
                                        ];


MyOutCoeff[fIn_, g_, rangeIn_, listSumVars_] := Module[
    {ftl},
    ftl = FactorTermsList[fIn, listSumVars];
    MyPrint[" in MyOutCoeff -> ", ftl,2];
    Times @@ Take[ftl, 2]*FSum[Times @@ Drop[ftl, 2]*g, rangeIn]
                                                      ];



RecOrderFct[rec_] := Module[
    {minOrder},
    minOrder = Cases[rec[[1]], SUM[a_] -> a, Infinity] // Min // Simplify; 
    Cases[rec[[1]], SUM[a_] -> a - minOrder, Infinity] // Max
                           ];

SelectRec[setOfRec_List] := Module[
    {tbl, j, min}, 
    tbl = Table[
        {setOfRec[[j]], 
        Sum[Length[(setOfRec[[j, 2]] + 1)[[i, 2]] + 1]
            ,{i, 2, Length[setOfRec[[j, 2]] + 1]}
           ]
        },{j, 1, Length[setOfRec]}
               ];
    min = Min[Table[tbl[[i, 2]], {i, 1, Length[setOfRec]}]];
    MyPrint["selecting the recurrence with small Delta parts", 2];
    Select[tbl, #[[2]] === min &][[1, 1]]
                                  ];



Arrange[rec_, range_]:=Module[
    {recIn, n,a,b,c,d, infinL},
    
    recIn=rec;
    
    infinL = Select[range, #[[3]]===Infinity&]//Length; (*infinite sums*)

    recIn = recIn
    /.F[n_,a_,b_]->F[n, Drop[{a,b},-infinL]//Reverse, Take[{a,b},-infinL]];

    recIn = recIn
    /.F[n_,a_,b_,c_,d_]->F[n, Drop[{a,b,c,d},-infinL]//Reverse, Take[{a,b,c,d},-infinL]];
    
    MyPrint["0-> ", recIn,2 ];

    recIn = recIn
    /.F[n_,a_,b_,c_]->F[n, Drop[{a,b,c},-infinL]//Reverse, Take[{a,b,c},-infinL]];

    MyPrint["1-> ", recIn,2];
    
    recIn/.F[n_,{a___},{b___}]->F[n,a,b]

                             ];


MySimplify[fIn_] := Module[
    {f, i, a, b},
    f = fIn;
    f = f /. Pochhammer[a_, b_] -> Gamma[a + b]/Gamma[a];
    f = f /. Binomial[a_, b_] -> Gamma[a + 1]/Gamma[b + 1]/Gamma[a - b + 1];
    f
                          ];



MyPrint[f__,level_Integer]:= If[level<=Global`$PrintConst,
        Print[f]
      ];

End[]


Protect[UseMultiSum, ShiftToPositive, SubstituteSummandInRec, CompensateTerms, NewSums, TransDeltas, AddDeltaTerms, InhRecurrence, SplitLargeCoeff, Arrange, Grade, FactorOutGCD, ShiftQuotient, MyPrint, MySimplify, RecOrderFct, MyOutCoeff, ExpandFinBSums, PlusTermsFct, FinRangeDeltas, DeltaList, ListOfRangeVars, TrivialSums, SelectRec];

EndPackage[]


If[$Notebooks,
   CellPrint[Cell[#, "Print", FontColor -> RGBColor[0, 0, 0], 
                              CellFrame -> 0.5, 
                              Background -> RGBColor[0.796887, 0.789075, 0.871107]]]&,
   Print
  ]["FSums package by Flavia Stan " <> 
    "\[LongDash] \[Copyright] RISC Linz \[LongDash] V " <> FSums`Private`Version];
