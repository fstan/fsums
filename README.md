# FSums

A Mathematica package for determining inhomogeneous recurrences
for nested sums over hypergeometric terms with nonstandard boundary conditions
by [Flavia Stan](http://flavias.net)

## Usage instructions

 - `UseMultiSum[fIn, N, listSumVars]` computes a recurrence by calling the
`FindRecurrence` and `FindStructureSet` functions from the package
[`MultiSum`](http://www.risc.jku.at/research/combinat/software/MultiSum/).
The list of summation variables can be reordered later by the `Arrange` functions.

 - `ShiftToPositive[rec, N, varList]` uses the `ShiftRecurrence` command from
`MultiSum` to shift the recurrence `rec` such that it contains only
positive shifts.

 - `Arrange[rec, range]` arranges the notation for the F's appearing in `rec`
such that we have `F[N, listSumVars]` where `listSumVars` is given by range.
This is used if the certificate recurrence was computed using an order of variables different from the one given by the range.

 - `InhRecurrence[rec, N, range, CoeffBound -> const]` delivers a inhomogeneous recurrence starting with the certificate recurrence `rec` in the variable `N`. If `const>0` the function `SplitLargeCoeff` is applied for large polynomial coefficients inside the delta parts.

    `CoeffBound`: Option for the `InhRecurrence` function. Default value is 0
to obtain the smallest number of FSums in the inhomogeneous part of the
recurrence.

 - `FSum[summand, range]` structure for the nested sums where the range is
given as a list of the form `{{sig, 0, Infinity}, {j,0,N-2}, {k,0,j+1},{l,0,N-k}}`.

 - `SubstituteSummandInRec[rec, fIn, N, listSumVars]` substitutes the summand
`fIn` as `F[N, listSumVars]` in the recurrence `rec`.


## Software requirements

The package works best in combination with the RISC Algorithmic Combinatorics Software Package MultiSum

   http://www.risc.jku.at/research/combinat/software/




